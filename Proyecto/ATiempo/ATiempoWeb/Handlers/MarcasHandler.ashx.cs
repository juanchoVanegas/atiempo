﻿using ATiempoPersistencia.Marcas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ATiempoWeb.Handlers
{
    /// <summary>
    /// Descripción breve de MarcasHandler
    /// </summary>
    public class MarcasHandler : IHttpHandler
    {
        
        private MarcaDelegado marca = new MarcaDelegado();
        private JavaScriptSerializer serializador = new JavaScriptSerializer();

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "application/json";

            //recibir la petición del cliente y analizar cuál opción requiere
            int opcion = 0;
            var parametros = context.Request.QueryString;
            string resultado = "";


            if (int.TryParse(parametros["opcion"], out opcion))
            {
                switch (opcion)
                {
                    case 1:  //Consultar todas las marcas

                        var lista = marca.consultarMarcas();
                        resultado = serializador.Serialize(lista);
                        context.Response.Write(resultado);
                        break;
                    case 2:  //Insertar una nueva Marca
                        int r = marca.insertarMarca(parametros["nombre"], parametros["logo"]);
                        if (r > 0)
                        {
                            resultado = serializador.Serialize( new { 
                                codigo = r, mensaje = "Marca agregada satisfactoriamente" 
                            });
                            context.Response.Write(resultado);
                        }

                        break;

                    default:
                        break;
                }
            }
            else {
                context.Response.Write("Error 500");
            }

            


            





        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}