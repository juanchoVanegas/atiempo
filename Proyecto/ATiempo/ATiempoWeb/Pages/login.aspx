﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ATiempoWeb.Pages.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Iniciar Sesión</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet" />

    <style type="text/css">
        .block-flat {
            margin-top: 100px;
            border-radius: 10px;
            padding: 10px 15px 6px 15px;
            border: solid #AAA 1px;
            min-height: 200px;
        }

        .naranja {
            color:#F90;

        }
    </style>

    <script src="../js/jquery-1.11.2.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

</head>
<body>

    <div id="contenedor" class="col-md-6 col-md-offset-3 block-flat">

        <form id="form1" runat="server">


            <div class="form-group">
                <label for="txtCorreo">Correo electrónico:</label>
                <asp:TextBox runat="server" ID="txtCorreo" ClientIDMode="Static" CssClass="form-control" />
            </div>

            <div class="form-group">
                <label for="txtPass">Contraseña:</label>
                <asp:TextBox runat="server" ID="txtPass" ClientIDMode="Static" TextMode="Password" CssClass="form-control" />
            </div>
            
            <div class="text-center" id="comandosLogin">
                <asp:Button Text="Ingresar" runat="server" ID="btnIngresar" CssClass="btn btn-success" OnClick="btnIngresar_Click" />
                <asp:Button Text="Registrar" runat="server" ID="btnRegistrar" CssClass="btn btn-primary" />
            </div>

            <div id="comandosRegistro" style="display:none;">
                <div class="form-group">
                    <label for="txtPassVerificar">Verificar Contraseña:</label>
                    <asp:TextBox runat="server" ID="txtPassVerificar" ClientIDMode="Static" TextMode="Password" CssClass="form-control" />
                </div>
                <div class="form-group">
                    <label for="txtNombre">Nombre:</label>
                    <asp:TextBox runat="server" ID="txtNombre" ClientIDMode="Static" CssClass="form-control" />
                </div>

                <div class="text-center">
                    <asp:Button Text="Enviar" runat="server" CssClass="btn btn-success" ID="btnEnviar" OnClick="btnEnviar_Click" />
                    <asp:Button Text="Cancelar" runat="server" CssClass="btn btn-danger" ID="btnCancelar" />
                </div>

            </div>

            <p id="pMensaje" class="naranja" runat="server"></p>

        </form>

    </div>

    <script type="text/javascript">


        $('#btnRegistrar').on('click', mostrarRegistro);
        $('#btnCancelar').on('click', ocultarRegistro);
        $('#btnEnviar').on('click', validarRegistro);
        $('#btnIngresar').on('click', validarLogin);

        function mostrarRegistro(e) {
            e.preventDefault();
            $('div#comandosLogin').slideUp('fast');
            $('div#comandosRegistro').slideDown('fast');
        }

        function ocultarRegistro(e) {
            e.preventDefault();
            $('div#comandosRegistro').slideUp('fast');
            $('div#comandosLogin').slideDown('fast');
        }

        function validarRegistro(e) {
            //validar que todas las cajas estén llenas
            var cajas = $('input[type="text"], input[type="password"]');

            var mensaje = '';
            var errores = 0;

            for (var i = 0; i < cajas.length; i++) {
                var caja = cajas[i];
                if (caja.value.trim()==='') {
                    mensaje += "El campo <strong>"+  $(caja).prev().text().replace(':','')  +"</strong> es obligatorio.<br />";
                    errores++;
                }
            }

            if ($('#txtPass').val().trim() !== $('#txtPassVerificar').val().trim()) {
                mensaje += "Las contraseñas no son iguales.<br />";
                errores++;
            }

            if (errores>0) {
                e.preventDefault();
                $('#pMensaje').html(mensaje);
            }
        }

        function validarLogin(e) {
            if ($('#txtCorreo').val().trim()==='' || $('#txtPass').val().trim()==='') {
                $('#pMensaje').text('El correo y la contraseña son obligatorios');
                e.preventDefault();
            }
        }


    </script>

</body>
</html>
