﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="ejemplos_jquery.aspx.cs" Inherits="ATiempoWeb.Pages.ejemplos_jquery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <div id="controles"></div>


</asp:Content>


<asp:Content ContentPlaceHolderID="scripts" runat="server" ID="scripts1"> 

    <script>
        
        var datos = [
            {
                id: 1,
                nombre: 'pepe',
                puntos:0
            },
            {
                id: 2,
                nombre: 'Nicolas',
                puntos: 0
            },
            {
                id: 3,
                nombre: 'Betty',
                puntos: 0
            },
            {
                id: 4,
                nombre: 'Felipe',
                puntos: 0
            }, {
                id: 5,
                nombre: 'Diana',
                puntos: 0
            }
        ];


        for (var i = 0; i < datos.length; i++) {
            var div = $('<div>').text(datos[i].nombre).attr('data-id', datos[i].id);
            var p = $('<p>').text('Puntos: ');
            var span = $('<span>').addClass('puntos').text('0');
            div.append(p);
            p.append(span);
            div.on('click', aumentar);
            $('#controles').append(div);
        }


        function aumentar() {

            var div = $(this);
            var span = div.find('.puntos');
            var puntos = parseInt( span.text() );
            puntos++;
            span.text(puntos);


            var id = $(this).attr('data-id');
            for (var i in datos) {
                if (datos[i].id==id) {
                    datos[i].puntos = puntos;
                }
            }

        };

    </script>


</asp:Content>
