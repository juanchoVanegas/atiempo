﻿using ATiempoPersistencia.Marcas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATiempoWeb.Pages
{
    public partial class Marca : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                cargarMarcas();                
            }

        }

        private void cargarMarcas()
        {
            var listaMarcas = new MarcaDelegado().consultarMarcas();

            gridMarcas.DataSource = listaMarcas;
            gridMarcas.DataBind();


            cmbMarcas.DataSource = listaMarcas;
            cmbMarcas.DataTextField = "Marca1";
            cmbMarcas.DataValueField = "idMarca";
            cmbMarcas.DataBind();




        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            int resultado =  new MarcaDelegado().insertarMarca(txtNombre.Text, txtLogo.Text);

            if (resultado > 0)
            {
                cargarMarcas();
            }
            else {
                Response.Write("ERROR");
            }

        }

        protected void cmbMarcas_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = int.Parse( cmbMarcas.SelectedValue );
            gridProductosMarca.DataSource = new MarcaDelegado().consultarProductosPorMarca(id);
            gridProductosMarca.DataBind();
        }
    }
}