﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="Marca.aspx.cs" Inherits="ATiempoWeb.Pages.Marca" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:GridView runat="server" ID="gridMarcas"></asp:GridView>

    <div>

        <asp:TextBox runat="server" ID="txtNombre" MaxLength="50" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="Este campo es Requerido" ControlToValidate="txtNombre"></asp:RequiredFieldValidator>


        <div>
            <asp:TextBox ID="txtCorreo" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                runat="server" ErrorMessage="Correo inválido"
                ControlToValidate="txtCorreo" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

        </div>




        <asp:TextBox runat="server" ID="txtLogo" MaxLength="50" />


        <asp:Button Text="Guardar" runat="server" ID="btnGuardar" OnClick="btnGuardar_Click" />

        <div>
            <h2>Consultar Productos por Marca</h2>
            <asp:DropDownList runat="server" ID="cmbMarcas" AutoPostBack="true" OnSelectedIndexChanged="cmbMarcas_SelectedIndexChanged">
            </asp:DropDownList>

            <asp:GridView runat="server" ID="gridProductosMarca"></asp:GridView>


        </div>

    </div>


</asp:Content>
