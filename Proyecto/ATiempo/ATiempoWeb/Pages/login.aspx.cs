﻿using ATiempoPersistencia;
using ATiempoPersistencia.Sesion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ATiempoWeb.Pages
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usuario"]!=null)
            {
                Response.Redirect("Marca.aspx");
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {

            if (txtCorreo.Text.Trim() == "" || txtPass.Text.Trim() == "" || txtPassVerificar.Text.Trim() == "" || txtNombre.Text.Trim() == "" || (txtPass.Text != txtPassVerificar.Text))
            {
                pMensaje.InnerText = "Error";
            }
            else {
                var delegado = new UsuarioDelegado();
                int resultado = delegado.agregarUsuario(txtCorreo.Text, txtPass.Text, txtNombre.Text);
                if (resultado>0)
                {
                    pMensaje.InnerText = "Operación exitosa";
                }
                else if (resultado == -1) {
                    pMensaje.InnerText = "El correo especificado ya está registrado";                
                }

            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            if (txtCorreo.Text.Trim() == "" || txtPass.Text.Trim() == "")
            {
                pMensaje.InnerText = "El correo y la contraseña son obligatorios";
            }
            else {
                var delegado = new UsuarioDelegado();
                Usuario usuario = delegado.validarUsuario(txtCorreo.Text, txtPass.Text);
                if (usuario != null)
                {
                    Session.Add("usuario", usuario);
                    Response.Redirect("Marca.aspx");
                }
                else {
                    pMensaje.InnerText = "Usuario o contraseña incorrectos";                    
                }
            }
        }
    }
}