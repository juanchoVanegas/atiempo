﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="ejemplos_ajax.aspx.cs" Inherits="ATiempoWeb.Pages.ejemplos_ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

        #marcas {
            
        }

        .item {
            vertical-align:top;
            width:150px;
            height:150px;
            display:inline-block;
            margin-right:7px;
            border:solid 1px #DDD;
        }

            .item img {
                width:90%;
            }


    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <img src="preloader.gif"  id="preloader" style="display:none;" />
     

    <div>
        <select id="lstMarcas" size="10">
        </select>
    </div>

    <div>

        <label for="txtNombre">Nombre: </label>
        <input type="text" id="txtNombre" value="" placeholder="Marca del equipo" />

        <label for="txtLogo">Logo: </label>
        <input type="text" id="txtLogo" value="" maxlength="20" />
        
    </div>

    <div>
        <button id="btnGuardar">Guardar</button>
        <button id="btnLimpiar">Limpiar</button>
    </div>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="scripts" runat="server">


    <script type="text/javascript">

        init();

        //se invoca cuando se ejecuta la página
        function init() {
            
            cargarMarcas();
            $('#btnGuardar').on('click', guardarMarca);
            $('#btnLimpiar').on('click', limpiar);


        }

        function cargarMarcas() {
            
            $.ajax({
                url: '../Handlers/MarcasHandler.ashx',
                data: { opcion: 1 },
                dataType: 'json',
                type: 'GET',
                error: onError,
                beforeSend: mostrarCargador,
                success: onConsultarCompleto
            });
        }

        function onError(_err) {
            console.error(_err);
        }

        function mostrarCargador() {
            $('#preloader').show();
        }

        function onConsultarCompleto(data) {
            $('#preloader').hide();

            var lista = $('#lstMarcas').html('');
            for (var i in data) {
                var item = data[i];
                var opcion = $('<option>').val(item.idMarca).text(item.Marca1);
                lista.append(opcion);
            }

        }

        function guardarMarca(e) {
            detenerEvento(e);
            $.ajax({
                url: '../Handlers/MarcasHandler.ashx',
                data: { opcion: 2, nombre:$('#txtNombre').val(), logo:$('#txtLogo').val() },
                dataType: 'json',
                type: 'GET',
                error: onError,
                beforeSend: mostrarCargador,
                success: onGuardarCompleto
            });
        }

        function onGuardarCompleto(data) {
            if (data.codigo === 1) {
                limpiar();
                cargarMarcas();
                alert(data.mensaje);
                
            } else {
                console.warn(data);
            }
        }

        function limpiar(e) {
            if (e) {
                detenerEvento(e);
            }
            $('input[type="text"]').val('');
        }



            function detenerEvento(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else if (e.stopPropagation) {
                    e.stopPropagation();
                } else {
                    e.returnValue = false;
                }
            }

    </script>

</asp:Content>
