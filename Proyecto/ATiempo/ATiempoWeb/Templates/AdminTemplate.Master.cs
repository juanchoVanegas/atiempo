﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ATiempoPersistencia;

namespace ATiempoWeb.Templates
{
    public partial class AdminTemplate : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["usuario"] != null)
            {
                spanNombre.InnerText = ((Usuario)Session["usuario"]).nombre;
                fechaIngreso.InnerText = "último ingreso " + ((Usuario)Session["usuario"]).fechaUltimoIngreso.ToShortDateString();
            }
            else {
                Response.Redirect("login.aspx");
            }



        }
    }
}