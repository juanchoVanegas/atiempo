﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATiempoPersistencia.Marcas
{
    //Esta clase debe ser pública para acceder a ella desde el proyecto WEB
    //Herede de la persistencia
    public class MarcaDelegado : ATiempoDBEntities
    {
        /*
         * insert
         * insertar varias
         * consulta
         * consulta por id
         * consulta por nombre
         * actualizar
         * 
         */



        /// <summary>
        /// Recibe dos string con el nombre y dirección URL del logo
        /// </summary>
        /// <param name="nombre">Nombre de la marca que se agregará</param>
        /// <param name="logo">URL del logo de la marca</param>
        /// <returns>Retorna la cantidad de filas afectdas</returns>
        public int insertarMarca(string nombre, string logo) {
            Marca.Add(new Marca() { Marca1 = nombre, logo = logo });
            return SaveChanges();
        }

        public int insertarMarca(List<Marca> marcas) {
            foreach (var marca in marcas)
            {
                Marca.Add(marca);
            }
            return SaveChanges();
        }

        public List<MarcaDTO> consultarMarcas(string nombre = "") {
            var lista = (from f in Marca
                         where f.Marca1.Contains(nombre)
                         select new MarcaDTO { 
                            idMarca = f.idMarca,
                            Marca1 = f.Marca1,
                            logo = f.logo
                         }).ToList();
            return lista;
        }

        public Marca consultarMarcas(int id) {
            var marca = (from f in Marca
                          where f.idMarca == id
                          select f).SingleOrDefault();
            return marca;
        }

        public int actualizarMarca(int id, string nombre, string logo) {

            var marca = (from f in Marca
                        where f.idMarca == id
                        select f).SingleOrDefault();

            if (marca != null)
            {
                marca.Marca1 = nombre;
                marca.logo = logo;
                return SaveChanges();
            }
            else {
                return -1;
            }


        }

        public List<spResumenProductosMarca_Result> consultarProductosPorMarca(int id) {
            return spResumenProductosMarca(id).ToList();
        }




    }
}
