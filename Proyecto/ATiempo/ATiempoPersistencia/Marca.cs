//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ATiempoPersistencia
{
    using System;
    using System.Collections.Generic;
    
    public partial class Marca
    {
        public Marca()
        {
            this.Producto = new HashSet<Producto>();
        }
    
        public int idMarca { get; set; }
        public string Marca1 { get; set; }
        public string logo { get; set; }
    
        public virtual ICollection<Producto> Producto { get; set; }
    }
}
