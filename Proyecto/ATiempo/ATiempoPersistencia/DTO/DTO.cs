﻿using ATiempoPersistencia;
namespace DTO {

    public sealed class ProductoDTO : Producto { }
    public sealed class MarcaDTO : Marca { }
    public sealed class UsuarioDTO : Usuario { }

    //y así para todas las entidades


}