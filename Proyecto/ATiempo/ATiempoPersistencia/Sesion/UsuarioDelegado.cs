﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATiempoPersistencia.Sesion
{
    public class UsuarioDelegado:ATiempoDBEntities
    {


        public int agregarUsuario(string correo, string pass, string nombre) {

            Usuario nuevo = new Usuario()
            {
                correo = correo,
                pass = pass,
                nombre = nombre,
                fechaRegistro = DateTime.Now,
                fechaUltimoIngreso = DateTime.Now
            };

            try
            {
                Usuario.Add(nuevo);
                return SaveChanges();
            }
            catch (DbUpdateException dbex)
            {
                return -1;
            }

            
        }

        public Usuario validarUsuario(string correo, string pass) {
            var usuario = (from f in Usuario
                           where f.correo.Equals(correo) && f.pass.Equals(pass, StringComparison.OrdinalIgnoreCase)
                           select f).SingleOrDefault();

            try
            {
                usuario.fechaUltimoIngreso = DateTime.Now;
                SaveChanges();
            }
            catch (Exception)
            {
            }


            return new Usuario() { 
                idUsuario=usuario.idUsuario, 
                nombre=usuario.nombre, 
                fechaUltimoIngreso=usuario.fechaUltimoIngreso,
                correo = usuario.correo
            };

        }



    }
}
