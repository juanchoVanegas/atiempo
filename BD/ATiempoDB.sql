USE [master]
GO
/****** Object:  Database [ATiempoDB]    Script Date: 02/27/2015 17:07:48 ******/
CREATE DATABASE [ATiempoDB] ON  PRIMARY 
( NAME = N'ATiempoDB', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\ATiempoDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ATiempoDB_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\ATiempoDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ATiempoDB] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ATiempoDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ATiempoDB] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ATiempoDB] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ATiempoDB] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ATiempoDB] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ATiempoDB] SET ARITHABORT OFF
GO
ALTER DATABASE [ATiempoDB] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ATiempoDB] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ATiempoDB] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ATiempoDB] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ATiempoDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ATiempoDB] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ATiempoDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ATiempoDB] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ATiempoDB] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ATiempoDB] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ATiempoDB] SET  DISABLE_BROKER
GO
ALTER DATABASE [ATiempoDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ATiempoDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ATiempoDB] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ATiempoDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ATiempoDB] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ATiempoDB] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ATiempoDB] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ATiempoDB] SET  READ_WRITE
GO
ALTER DATABASE [ATiempoDB] SET RECOVERY SIMPLE
GO
ALTER DATABASE [ATiempoDB] SET  MULTI_USER
GO
ALTER DATABASE [ATiempoDB] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ATiempoDB] SET DB_CHAINING OFF
GO
USE [ATiempoDB]
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estados](
	[idEstado] [int] IDENTITY(1,1) NOT NULL,
	[Estado] [nchar](20) NOT NULL,
	[Detalles] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[idEstado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedido]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedido](
	[idPedido] [int] IDENTITY(1,1) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[idCliente] [int] NOT NULL,
	[Total] [money] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[DireccionDestino] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Pedido] PRIMARY KEY CLUSTERED 
(
	[idPedido] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marca]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marca](
	[idMarca] [int] IDENTITY(1,1) NOT NULL,
	[Marca] [nvarchar](50) NOT NULL,
	[logo] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Marca] PRIMARY KEY CLUSTERED 
(
	[idMarca] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[correo] [nvarchar](50) NOT NULL,
	[pass] [nvarchar](50) NOT NULL,
	[fechaRegistro] [datetime] NOT NULL,
	[fechaUltimoIngreso] [datetime] NOT NULL,
	[nombre] [nvarchar](70) NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[Rol] [nchar](20) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[idProducto] [int] IDENTITY(1,1) NOT NULL,
	[idMarca] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Precio] [money] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descripcion] [nvarchar](250) NOT NULL,
	[imagen] [nvarchar](50) NOT NULL,
	[Garantia] [tinyint] NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permisos]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permisos](
	[idPermiso] [int] IDENTITY(1,1) NOT NULL,
	[idRol] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[FechaAsignacion] [date] NOT NULL,
 CONSTRAINT [PK_Permisos] PRIMARY KEY CLUSTERED 
(
	[idPermiso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistorialPedido]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistorialPedido](
	[idHistorialPedido] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NOT NULL,
	[idEstado] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_HistorialPedido] PRIMARY KEY CLUSTERED 
(
	[idHistorialPedido] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetallePedido]    Script Date: 02/27/2015 17:07:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetallePedido](
	[idDetallePedido] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NOT NULL,
	[idProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [money] NOT NULL,
 CONSTRAINT [PK_DetallePedido] PRIMARY KEY CLUSTERED 
(
	[idDetallePedido] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Producto_Marca]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Marca] FOREIGN KEY([idMarca])
REFERENCES [dbo].[Marca] ([idMarca])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Marca]
GO
/****** Object:  ForeignKey [FK_Permisos_Rol]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[Permisos]  WITH CHECK ADD  CONSTRAINT [FK_Permisos_Rol] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[Permisos] CHECK CONSTRAINT [FK_Permisos_Rol]
GO
/****** Object:  ForeignKey [FK_Permisos_Usuario]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[Permisos]  WITH CHECK ADD  CONSTRAINT [FK_Permisos_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Permisos] CHECK CONSTRAINT [FK_Permisos_Usuario]
GO
/****** Object:  ForeignKey [FK_HistorialPedido_Estados]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[HistorialPedido]  WITH CHECK ADD  CONSTRAINT [FK_HistorialPedido_Estados] FOREIGN KEY([idEstado])
REFERENCES [dbo].[Estados] ([idEstado])
GO
ALTER TABLE [dbo].[HistorialPedido] CHECK CONSTRAINT [FK_HistorialPedido_Estados]
GO
/****** Object:  ForeignKey [FK_HistorialPedido_Pedido]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[HistorialPedido]  WITH CHECK ADD  CONSTRAINT [FK_HistorialPedido_Pedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[Pedido] ([idPedido])
GO
ALTER TABLE [dbo].[HistorialPedido] CHECK CONSTRAINT [FK_HistorialPedido_Pedido]
GO
/****** Object:  ForeignKey [FK_HistorialPedido_Usuario]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[HistorialPedido]  WITH CHECK ADD  CONSTRAINT [FK_HistorialPedido_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[HistorialPedido] CHECK CONSTRAINT [FK_HistorialPedido_Usuario]
GO
/****** Object:  ForeignKey [FK_DetallePedido_Pedido]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[DetallePedido]  WITH CHECK ADD  CONSTRAINT [FK_DetallePedido_Pedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[Pedido] ([idPedido])
GO
ALTER TABLE [dbo].[DetallePedido] CHECK CONSTRAINT [FK_DetallePedido_Pedido]
GO
/****** Object:  ForeignKey [FK_DetallePedido_Producto]    Script Date: 02/27/2015 17:07:50 ******/
ALTER TABLE [dbo].[DetallePedido]  WITH CHECK ADD  CONSTRAINT [FK_DetallePedido_Producto] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Producto] ([idProducto])
GO
ALTER TABLE [dbo].[DetallePedido] CHECK CONSTRAINT [FK_DetallePedido_Producto]
GO
